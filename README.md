# numanalysis-demo
Numerical Analysis :: Algorithm Demos (Gauss Jordan Inverse, Jacobi... and other asap)


## Live Demo
Check out the Live Demo on [**https://num-analysis.work**](https://num-analysis.work) to use the above algorithms.


## Support
If you need any support or have any questions, please reach out via email at fabrizio.armango@gmail.com.


## Roadmap
- [x] angular migration from v2 to v15
- [ ] add sample input for implemented algorithms
- [ ] add constraints to inputs
- [ ] implement CholeskyMethod
- [ ] implement GaussElimination
- [ ] write tests
- [ ] run tests using GitLab CI


## Contributing
This project welcomes contributions. Feel free to create issues, add features, stories, fix bugs or anything you think could help the community.


## Project status
The project is currently in beta. 