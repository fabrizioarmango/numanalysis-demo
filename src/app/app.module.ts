import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { GaussJordanInverseComponent } from './gauss-jordan-inverse/gauss-jordan-inverse.component';
import { GaussSeidelComponent } from './gauss-seidel-method/gauss-seidel-method.component';
import { JacobiMethodComponent } from './jacobi-method/jacobi-method.component';
import { CholeskyMethodComponent } from './cholesky-method/cholesky-method.component';

import { MatrixComponent } from './matrix/matrix.component';
import { MatrixCellComponent } from './matrix/matrix-cell/matrixcell.component';

@NgModule({
  declarations: [
    AppComponent,
    GaussJordanInverseComponent,
    JacobiMethodComponent,
    GaussSeidelComponent,
    CholeskyMethodComponent,
    MatrixComponent,
    MatrixCellComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }