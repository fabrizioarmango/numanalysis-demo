import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'matrix-cell',
  templateUrl: './matrixcell.component.html'
})

export class MatrixCellComponent implements OnInit
{
  @Input() value: string = "";
  @Input() disabled: boolean = false;
  id: string;

  constructor() {
    this.id = "";
  }

  ngOnInit(): void {
      
  }
}
