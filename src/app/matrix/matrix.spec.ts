import { Matrix } from "./matrix";

describe('Matrix class', () => {
    const numRows: number = 2;
    const numColumns: number = 2;
    let matrix: Matrix;

    it(`should have expected sizes`, () => {
        matrix = new Matrix(numRows, numColumns);
        expect(matrix.rows).toBe(numRows);
        expect(matrix.columns).toBe(numColumns);
    });

    describe('.getAt(i, j)', () => {
        matrix = new Matrix(numRows, numColumns);
        it(`gets the value in the entry [i, j]`, () => {
            const expected = 1;
            matrix.setAt(0, 0, expected);
            expect(matrix.getAt(0, 0)).toBe(expected);
        });
    });

    describe('.setAt(i, j, v)', () => {
        matrix = new Matrix(numRows, numColumns);
        it(`sets the value v in the entry [i, j]`, () => {
            matrix.setAt(0, 0, 1);
            expect(matrix.elements[0][0]).toBe(1);
        });
    });

    describe('.CloneFrom(input)', () => {
        matrix = new Matrix(numRows, numColumns);
        /*
        matrix:
            | 2 1 | 
            | 1 2 |
        */
        matrix.setAt(0, 0, 2);
        matrix.setAt(0, 1, 1);
        matrix.setAt(1, 0, 1);
        matrix.setAt(1, 1, 2);

        it(`clones every entry from input matrix`, () => {
            const clonedMatrix = Matrix.CloneFrom(matrix);
            for (var i = 0; i < matrix.rows; i++)
                for (var j = 0; j < matrix.columns; j++) {
                    expect(clonedMatrix.getAt(i, j)).toBe(clonedMatrix.getAt(i, j));
                }
        });
    });

    describe('.Identity', () => {
        const expectedSize = 2;
        matrix = Matrix.Identity(expectedSize);
        it(`instantiates a matrix with expected size`, () => {
            expect(matrix.rows).toBe(expectedSize);
            expect(matrix.columns).toBe(expectedSize);
        });

        it(`instantiates a matrix filled with ones`, () => {
            for (var i = 0; i < matrix.rows; i++)
                for (var j = 0; j < matrix.columns; j++) {
                    expect(matrix.getAt(i, j)).toBe(1);
                }
        });

    });

    describe('.ZeroMatrix', () => {
        const expectedSize = 2;
        matrix = Matrix.ZeroMatrix(expectedSize);
        it(`instantiates a matrix with expected size`, () => {
            expect(matrix.rows).toBe(expectedSize);
            expect(matrix.columns).toBe(expectedSize);
        });

        it(`instantiates a matrix filled with zeroes`, () => {
            for (var i = 0; i < matrix.rows; i++)
                for (var j = 0; j < matrix.columns; j++) {
                    expect(matrix.getAt(i, j)).toBe(0);
                }
        });

    });

});