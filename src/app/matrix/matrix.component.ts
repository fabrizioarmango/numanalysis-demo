import { Component, Input, OnInit } from '@angular/core';
import { Matrix } from './matrix';

@Component({
  selector: 'matrix-view',
  templateUrl: './matrix.component.html'
})
export class MatrixComponent implements OnInit
{
    @Input() rows!: number;
    @Input() columns!: number;
    @Input('matrix') matrixInput: Matrix | undefined = undefined;
    @Input() disabled: boolean = false;
    matrix: Matrix | undefined;

    ngOnInit(): void {}
  
    ngOnChanges()
    {

      if (this.matrixInput === undefined)
      { /* not initialized */
        console.log(this.rows, this.columns);
        this.matrix = new Matrix(this.rows, this.columns);
        console.log("Inizializza: ", this.matrix);
      } else
        this.matrix = this.matrixInput;
    }

    updateMatrixValues(i: number, j: number, event: any): void
    {
      if (undefined == this.matrix) {
        throw new Error("Matrix has not been defined yet.");
      }
      var target = event.target || event.srcElement || event.currentTarget;
      var value = target.value;
      this.matrix.setAt(i, j, value);
    }

    createRange(n: number): number[]
    {
      var items: number[] = [];
      for(var i = 1; i <= n; i++)
      {
         items.push(i);
      }
      return items;
    }

    isNotValidMatrixSize(): boolean
    {
      return !(this.rows && this.columns && this.rows > 0 && this.columns > 0);
    }
}
