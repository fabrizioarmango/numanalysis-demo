import { Component } from '@angular/core'
import { Matrix } from '../matrix/matrix';
import { MatrixPrint } from '../matrix/matrixprint.service';

@Component({
  selector: 'cholesky-method',
  templateUrl: './cholesky-method.component.html',
  providers: [MatrixPrint]
})

// status: wip
export class CholeskyMethodComponent {
  rows!: number;
  columns!: number;

  inputMatrix: Matrix | undefined;
  matrixL: Matrix | undefined;

  getTitleOf(i: number): Array<any> {
    return this.loggingService.logTitles[i];
  }

  getDescOf(i: number): Array<any> {
    return this.loggingService.logDescriptions[i];
  }

  getMatrices(): Array<any> {
    return this.loggingService.logMatrices;
  }

  _sizeSpoilerVisible: boolean = true;
  sizeSpoilerVisible(): Boolean {
    return this._sizeSpoilerVisible;
  }

  switchSizeSpoilerVisibility(): void {
    this._sizeSpoilerVisible = !this._sizeSpoilerVisible;
  }

  _calcButtonHidden: boolean = false;
  calcButtonHidden(): Boolean {
    return this._calcButtonHidden;
  }

  isNotValidMatrixSize(): boolean {
    return !(this.rows && this.columns && this.rows > 0 && this.columns > 0);
  }

  constructor(private loggingService: MatrixPrint) { }

  onClick(matrix: Matrix): void {
    this.algorithm(matrix);
  }

  algorithm(A: Matrix) {
    this.inputMatrix = Matrix.CloneFrom(A);

    this.matrixL = Matrix.ZeroMatrix(this.inputMatrix.rows);

    for (let j: number = 0; j < this.matrixL.rows; j++) {
      for (let i: number = j; i < this.matrixL.rows; i++) {
        console.debug("CURR::i: " + i + ", j: " + j);
        this.matrixL.setAt(i, j, this.calcElementOfL(i, j));

      }
    }
    this.Log("Output L", this.matrixL, "test");
  }

  calcElementOfL(i: number, j: number): number {
    if (this.inputMatrix == undefined)
      throw new Error('Input Matrix is not defined.');
    if (this.matrixL == undefined)
      throw new Error('Input matrixL is not defined.');
      
    let value: number = 0;
    console.debug("calcElementOfL(" + i + ", " + j + ")");
    if (i == j)
      value = Math.sqrt(this.inputMatrix.getAt(j, j) - this.sumLQ(j));
    else {
      value = this.inputMatrix.getAt(i, j) - this.sumLL(i, j);
      value /= this.matrixL.getAt(j, j);
    }
    return value;
  }

  sumLQ(j: number): number {
    if (this.matrixL == undefined)
      throw new Error('Input matrixL is not defined.');
    let value = 0;
    for (let k = 0; k < j; k++) {
      value += Math.pow(this.matrixL.getAt(j, k), 2);
      console.debug("sumLQ(" + j + ").value: " + value);
    }
    return value;
  }

  sumLL(i: number, j: number): number {
    if (this.matrixL == undefined)
      throw new Error('Input matrixL is not defined.');
    let value = 0;
    for (let k = 0; k < j; k++) {
      value += this.matrixL.getAt(i, k) * this.matrixL.getAt(j, k);
      console.debug("sumLL(" + i + ", " + j + ").value: " + value);
    }
    return value;
  }

  Log(title: string, matrix: Matrix, description: string): void {
    this.loggingService.log(title, matrix, description);
  }

}