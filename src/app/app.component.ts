import { Component, ViewEncapsulation } from '@angular/core';

type Algorithm = {
    id: number;
    name: string;
};

@Component({
    selector: 'numerical-analysis-demo',
    templateUrl: './app.component.html',
    styleUrls: ['../css/app.css']
})
export class AppComponent {
    title = 'Numerical Analysis';
    algorithms: Algorithm[] = [
        { id: 1, name: 'Gauss-Jordan Inverse' },
        { id: 2, name: 'Jacobi Method' },
        { id: 3, name: 'Gauss-Seidel' },
        { id: 4, name: 'Cholesky Method' }
    ];
    selectedAlgorithm: Algorithm | undefined;
    currentYear: string = new Date().getFullYear().toString();
    showSidebar: boolean = false;

    onSidebarToggleClick(): void {
        this.showSidebar = !this.showSidebar;
    }

    onSelect(algorithm: Algorithm): void {
        this.selectedAlgorithm = algorithm;
    }
}