
import { GaussSeidelComponent } from "./gauss-seidel-method.component";
import { NO_ERRORS_SCHEMA } from '@angular/core'
import { TestBed, ComponentFixture } from '@angular/core/testing'

let component: GaussSeidelComponent;
let fixture: ComponentFixture<GaussSeidelComponent>;

describe('GaussSeidelComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [GaussSeidelComponent],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents()
            .then(async () => {
                fixture = TestBed.createComponent(GaussSeidelComponent);
                component = fixture.componentInstance;

                // change detection triggers ngOnInit which gets a hero
                fixture.detectChanges();
                await fixture.whenStable();
                // got the heroes and updated component
                // change detection updates the view
                fixture.detectChanges();
            });
    });


    it('should display heroes', () => {
        // expect(page.heroRows.length).toBeGreaterThan(0);
        expect(1).toBeGreaterThan(0);
    });

    

});