import { Component } from '@angular/core';
import { GaussJordanInverseComponent } from '../gauss-jordan-inverse/gauss-jordan-inverse.component';
import { Matrix } from '../matrix/matrix';
import { MatrixPrint } from '../matrix/matrixprint.service';

@Component({
  selector: 'gauss-seidel-method',
  templateUrl: './gauss-seidel-method.component.html',
  providers: [MatrixPrint]
})

export class GaussSeidelComponent {
  rows!: number;
  columns!: number;

  solutionX: Array<any> = [];

  M_inv: Matrix;
  _calcButtonVisibility: boolean = false;

  _sizeSpoilerVisible: boolean = true;

  sampleMatrix: Matrix | undefined;
  sampleConstantVector: Matrix | undefined;
  sampleStartVector: Matrix | undefined;

  sizeSpoilerVisible(): Boolean {
    return this._sizeSpoilerVisible;
  }

  switchSizeSpoilerVisibility(): void {
    this._sizeSpoilerVisible = !this._sizeSpoilerVisible;
  }

  _calcButtonHidden: boolean = false;
  calcButtonHidden(): Boolean {
    return this._calcButtonHidden;
  }

  Log(title: string, matrix: Matrix, description: string): void {
    this.loggingService.log(title, matrix, description);
  }

  getTitleOf(i: number): Array<any> {
    return this.loggingService.logTitles[i];
  }

  getDescOf(i: number): Array<any> {
    return this.loggingService.logDescriptions[i];
  }

  getMatrices(): Array<any> {
    return this.loggingService.logMatrices;
  }

  constructor(private loggingService: MatrixPrint) {
    this.M_inv = new Matrix(0, 0);
  }

  onSampleButtonClick() {
    this.sampleMatrix = Matrix.FromEntries([
      [2, 1],
      [1, 2]
    ]);
    this.rows = this.sampleMatrix.rows;
    this.columns = this.sampleMatrix.columns;

    this.sampleConstantVector = Matrix.FromEntries([
      [2],
      [2]
    ]);

    this.sampleStartVector = Matrix.FromEntries([
      [0],
      [0]
    ]);

    console.log("onSampleButtonClick", this.sampleMatrix, this.rows, this.columns);
  }

  onCalcButtonClick(matrix: Matrix, constantV: Matrix, vectorX: Matrix): void {
    this._calcButtonVisibility = true;

    this.algorithm(matrix, constantV, vectorX, 3);
  }


  algorithm(inputM: Matrix, inputV: Matrix, inputX: Matrix, iterations: number): void {
    this.solutionX = [];
    this.solutionX.push(inputX);

    let d: Matrix = new Matrix(inputV.rows, inputV.columns);
    let Bgs: Matrix = this.getGaussSeidelMatrix(inputM);

    this.Log("Bgs Matrix", Bgs, "Matrice di Gauss Seidel");
    this.Log("Inverse of (D-B)", this.M_inv, " ");

    d = Matrix.CloneFrom(Matrix.Multiply(Bgs, inputV));
    this.Log("Vector d", d, " ");

    let iteration: number = 0;
    let solution: Matrix;
    while (iteration < iterations) {
      solution = Matrix.Multiply(Bgs, this.solutionX[iteration]);
      solution = Matrix.SumBetween(solution, d);
      this.solutionX.push(solution);
      this.Log("X(" + iteration + ")", this.solutionX[iteration], " ");
      iteration++;
    }

  }

  getGaussSeidelMatrix(A: Matrix): Matrix {
    let D: Matrix = Matrix.DiagonalMatrixOf(A);
    console.log("D Matrix", D);

    let B: Matrix = Matrix.LowerMatrixOf(A, false);
    console.log("B Matrix", B);

    let C: Matrix = Matrix.UpperMatrixOf(A, false);
    console.log("C Matrix", C);

    this.Log("D Matrix", D, "...");
    this.Log("B Matrix", B, "...");
    this.Log("C Matrix", C, "...");
    // B*(-1)
    // B.scalarMultiply(-1);
    // this.Log("-B Matrix", B, "...");

    // let M: Matrix = Matrix.SumBetween(D, B);
    let M: Matrix = Matrix.Substraction(D, B);

    this.Log("M Matrix", M, "...");

    this.M_inv = (M.rows == 2) ? M.inverse2x2() : GaussJordanInverseComponent.algorithm(M);

    this.Log("M_inv Matrix", this.M_inv, "...");

    let Bgs: Matrix = Matrix.Multiply(this.M_inv, C);

    return Bgs;
  }
  /*
  static GetGaussSeidelIteration(A: Matrix, vectorX: Matrix, iteration: number): Matrix
  {
      var x_i = b[i][0] - (sum1*vectorX[j][0] + sum2*vectorX[][])
  }
  */
  isNotValidMatrixSize(): boolean {
    return !(this.rows && this.columns && this.rows > 0 && this.columns > 0);
  }

  isNotValidVectorSize(): boolean {
    return !((this.rows && this.columns) && (this.rows > 0));
  }

  createRange(n: number) {
    var items: number[] = [];
    for (var i = 1; i <= n; i++) {
      items.push(i);
    }
    return items;
  }
}