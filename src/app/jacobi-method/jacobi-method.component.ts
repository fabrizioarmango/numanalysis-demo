import { Component, NgModule } from '@angular/core';
import { Matrix } from '../matrix/matrix';
import { GaussJordanInverseComponent } from '../gauss-jordan-inverse/gauss-jordan-inverse.component';
import { MatrixPrint } from '../matrix/matrixprint.service';

@Component({
  selector: 'jacobi-method',
  templateUrl: './jacobi-method.component.html',
  providers: [MatrixPrint]
})

export class JacobiMethodComponent {
  rows!: number;
  columns!: number;

  Log(title: string, matrix: Matrix, description: string): void {
    this.loggingService.log(title, matrix, description);
  }

  getTitleOf(i: number): Array<any> {
    return this.loggingService.logTitles[i];
  }

  getDescOf(i: number): Array<any> {
    return this.loggingService.logDescriptions[i];
  }

  getMatrices(): Array<any> {
    return this.loggingService.logMatrices;
  }

  constructor(private loggingService: MatrixPrint) { }

  onClick(matrix: Matrix, constantV: Matrix, vectorX: Matrix): void {
    console.debug("Arrivo qua");
    this.algorithm(matrix, constantV, vectorX, 3);
  }


  algorithm(inputM: Matrix, inputV: Matrix, inputX: Matrix, iterations: number): void {
    var Bj: Matrix = JacobiMethodComponent.GetJacobiMatrix(inputM);
    this.Log("Jacobi Matrix", Bj, "  ");

    var dV: Matrix = new Matrix(inputM.columns, 1);

    for (var i: number = 0; i < dV.rows; i++) {
      var value: number = inputV.getAt(i, 0) / inputM.getAt(i, i);
      console.log("value: " + value);
      dV.setAt(i, 0, value);
    }
    this.Log("Vector d", dV, " ");

    var outputV: Matrix = Matrix.CloneFrom(inputX);

    for (var i: number = 0; i < iterations; i++) {
      console.log("iterazione: " + i);
      outputV = Matrix.Multiply(Bj, outputV);
      outputV = Matrix.CloneFrom(outputV);
      outputV = Matrix.SumBetween(outputV, dV);
      this.Log("x(" + i + "):", outputV, "iteration:" + (i + 1));
    }
  }

  static GetJacobiMatrix(A: Matrix): Matrix {
    var J: Matrix = new Matrix(A.rows, A.columns);

    for (var i: number = 0; i < A.rows; i++)
      for (var j: number = 0; j < A.columns; j++) {
        if (i == j)
          J.setAt(i, j, 0);
        else
          J.setAt(i, j, -A.getAt(i, j) / A.getAt(i, i));
      }

    return J;
  }

  isNotValidMatrixSize(): boolean {
    return !(this.rows && this.columns && this.rows > 0 && this.columns > 0);
  }

  isNotValidVectorSize(): boolean {
    return !((this.rows && this.columns) && (this.rows > 0));
  }

  createRange(n: number) {
    var items: number[] = [];
    for (var i = 1; i <= n; i++) {
      items.push(i);
    }
    return items;
  }
}